# See ~/local/tools/supported_python_versions_pip.py for helper script
# python ~/local/tools/supported_python_versions_pip.py matplotlib
# python ~/local/tools/supported_python_versions_pip.py opencv-python-headless
# python ~/local/tools/supported_python_versions_pip.py matplotlib
# python ~/local/tools/supported_python_versions_pip.py ubelt

six >= 1.11.0

ubelt>=1.2.0     ;                            python_version >= '3.6'    # Python 3.6+

matplotlib>=3.5.0     ;                            python_version >= '3.10'    # Python 3.10+
matplotlib>=3.4.0     ; python_version < '3.10' and python_version >= '3.9'    # Python 3.9
matplotlib>=3.4.0     ; python_version < '3.9' and python_version >= '3.8'    # Python 3.8
matplotlib>=3.4.0     ; python_version < '3.8' and python_version >= '3.7'    # Python 3.7
matplotlib>=3.1.0     ; python_version < '3.7' and python_version >= '3.6'    # Python 3.6

# We can't do one or the other. Kind of annoying
# opencv-python >= 3.4.1.15
# opencv-python-headless >= 3.4.1.15

kwarray >= 0.5.19
kwimage >= 0.7.14
